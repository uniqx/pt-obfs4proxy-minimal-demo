# PT obfs4proxy minimal demo

This project illustrates a minimal setup for using the pluggable transports implementation obfs4proxy as a standalone proxy.

## Overview

This is the high level overview of what this project implements:

    +-----------------------+            +-----------------------+
    |                       |            |                       |
    | Server App            | plain tcp  | PT Server             |
    |                       |----------->|                       |
    | python -m http.server |<-----------| obfs4_server.sh       |
    | (:8000)               |            | (:9876)               |
    |                       |            |                       |
    +-----------------------+            +-----------------------+
       ^                                                  ^ |
       |                                                  | |
       | [http over obfs4 *]                       obfs4  | |
       |                                                  | |
       v                                                  | v
    +-----------------------+            +-----------------------+
    |                       | http over  |                       |
    | Client App            |   socks5   | PT Client             |
    |                       |<-----------|                       |
    | client_app.py         |----------->| obfs4_client.sh       |
    |                       |            | (:auto)               |
    |                       |            |                       |
    +-----------------------+            +-----------------------+


\* There never is a direct connection between Client App and Server App,
this is just for clarifying where the HTTP endpoints are.


## do test run

    # init vm for test run
    vagrant up

    # start server app
    vagrant ssh -c 'nohup bash -c "cd /vagrant && python3 -m http.server &"'

    # start pt server
    vagrant ssh -c 'nohup bash -c "bash /vagrant/obfs4_server.sh &"'

    # start pt client
    vagrant ssh -c 'nohup bash -c "bash /vagrant/obfs4_client.sh &"'

    # do client call
    vagrant ssh -c 'python3 /vagrant/client_app.py'

(Note: only tested with `--provider libvirt`. Might need some tweaks to work with virtualbox or other VM hypervisors)

## client app

In this szenario the client app could also be a simple curl invocation like this:

    curl --proxy "socks5://cert=g1zoXZNOcxgZZcKMtk9ReIZVugDYiWDbAGRItNRPGC4ETKW2Zpy9kLmySrR/TuRwSmb3DQ;iat-mode=0:0@127.0.0.1:39829" 127.0.0.1:9876

## Credits

* https://www.pluggabletransports.info
* https://github.com/Yawning/obfs4
