#! /bin/bash

export TOR_PT_MANAGED_TRANSPORT_VER=1
export TOR_PT_STATE_LOCATION=/vagrant/pt_client_state
export TOR_PT_EXIT_ON_STDIN_CLOSE=0
export TOR_PT_CLIENT_TRANSPORTS=obfs4

obfs4proxy -enableLogging -logLevel=DEBUG
