#! /bin/bash

export TOR_PT_MANAGED_TRANSPORT_VER=1
export TOR_PT_STATE_LOCATION=/vagrant/pt_server_state
export TOR_PT_EXIT_ON_STDIN_CLOSE=0
export TOR_PT_SERVER_TRANSPORTS=obfs4
export TOR_PT_SERVER_BINDADDR=obfs4-0.0.0.0:9876
export TOR_PT_ORPORT=127.0.0.1:8000

obfs4proxy -enableLogging -logLevel=DEBUG
