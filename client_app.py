#! /usr/bin/env python3

import re
import socks
import socket
import urllib.request


def main():

    # prepare connection arguments
    http_args = {'addr': '127.0.0.1',
                 'port': '9876'}

    socks5_args = {'user': parse_args_from_bridgeline('/vagrant/pt_server_state/obfs4_bridgeline.txt'),
                   'pw': "\x00",
                   'host': '127.0.0.1',
                   'port': parse_client_port_from_log('/vagrant/pt_client_state/obfs4proxy.log')}
    remote_dns = False

    # force all sockets of this script to use socks5 proxy
    socks.set_default_proxy(socks.SOCKS5,
                            socks5_args['host'],
                            socks5_args['port'],
                            remote_dns,
                            socks5_args['user'],
                            socks5_args['pw'])
    socket.socket = socks.socksocket

    # do http request
    r = urllib.request.urlopen('http://{addr}:{port}'.format(addr=http_args['addr'],
                                                             port=http_args['port']))
    # print http response
    print(r.read().decode())



def parse_args_from_bridgeline(pt_server_bridgeline_path):
    """parse arguments from pt server bridgeline file and return them
    in the correct format for being useable to make a socks connection
    to that bridge

    for a file contianing:

        Bridge obfs4 <IP ADDRESS>:<PORT> <FINGERPRINT> cert=g1zoXZNOcxgZZcKMtk9ReIZVugDYiWDbAGRItNRPGC4ETKW2Zpy9kLmySrR/TuRwSmb3DQ iat-mode=0

    this function will return:

        cert=g1zoXZNOcxgZZcKMtk9ReIZVugDYiWDbAGRItNRPGC4ETKW2Zpy9kLmySrR/TuRwSmb3DQ;iat-mode=0
    """
    with open(pt_server_bridgeline_path, 'r') as f:
        for line in f.readlines():
            if line.startswith('Bridge obfs4'):
                return ';'.join(line.split()[5:])


def parse_client_port_from_log(pt_client_log_path):
    """parse last know client port from pt client logs and return it"""
    with open(pt_client_log_path, 'r') as f:
        last_known_port = 0
        for line in f.readlines():
            m = re.match('.*obfs4 - registered listener: .*:(?P<port>[0-9]+)$',
                         line)
            if m:
                last_known_port = int(m.group('port'))
        return last_known_port


if __name__ == '__main__':
    main()
